import java.util.Scanner;

public class CalificacionYPromedio {
    public void main(String[] args){
        resultadosCalific();

    }

    int[] calificaciones = {80, 75, 50, 90, 100};


    // Métodos
    public int promedioCalificaciones() {
        return (calificaciones[0] + calificaciones[1] + calificaciones[2] + calificaciones[3] + calificaciones[4]) / 5;
    }

    public char calificacionFinal(int promedio) {

        char calif;

        if (promedio <= 50) {
            calif = 'F';
        }
        else if (promedio <= 60) {
            calif = 'E';
        }
        else if (promedio <= 70) {
            calif = 'D';
        }
        else if (promedio <= 80) {
            calif = 'C';
        }
        else if (promedio <= 90) {
            calif = 'B';
        }
        else {
            calif = 'A';
        }
        return calif;
    }

    public void resultadosCalific() {

        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Nombre del alumno:");
        String nombreAlumno = myObj.nextLine();  // Read user input

        System.out.println("Nombre del estudiante: " + nombreAlumno);  // Output user input
        System.out.println("Calificación 1: " + calificaciones[0]);
        System.out.println("Calificación 2: " + calificaciones[1]);
        System.out.println("Calificación 3: " + calificaciones[2]);
        System.out.println("Calificación 4: " + calificaciones[3]);
        System.out.println("Calificación 5: " + calificaciones[4]);
        System.out.println("Promedio: " + promedioCalificaciones());
        System.out.println("Calificación: " + calificacionFinal(promedioCalificaciones()));


    }
    


}
